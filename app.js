const express = require('express');
const bodyparser = require('body-parser');
const mongodb = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
//const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const app = express();

const connection = (closure) => {
    return mongodb.connect('mongodb://localhost:27017', (err, client) => {
        if (err) throw err;
        let db = client.db('kunden');
        closure(db);
    })
}

app.use(bodyparser.json());

app.get('/', (req, res) => {
    connection(async (db) => {
        const data = await db.collection("user").find().toArray();
        res.send({ data: data });
    })

});

app.post('/register', (req, res) => {
    connection(async (db) => {
        const result = await db.collection("user").insertOne(req.body);
        res.send({ data: result });
    })
})

app.post('/login', (req, res) => {
    connection(async (db) => {
        const user = await db.collection("user").findOne({ email: req.body.email });
        if (!user) {
            res.send({ message: "user not found" });
        }

        if (user.password !== req.body.password) {
            res.send({ message: "wrong email" });
        }
        const token = jwt.sign({ data: user }, 'secret_pass')
        res.send({ message: "ok", token: token })
    })
})

//app.post('/todo/:id',(req, res)=>{
// connection(async (db)=>{
//      const user = await db.collection("user").updateOne(req.params.id,{ $push: req.body.note } );
//      res.send({ data: result });
// res.send('Displaying information for uid ' + req.params.id);
//  })
//})


app.post('/todo/:id/:i', (req, res) => {
    const qq = { _id: ObjectId(req.params.id) };
    const i = req.params.i;
    connection(async (db) => {
        const user = await db.collection("user").updateOne(qq, { $set: { ["todo." + i]: req.body } });
        res.send({ data: user });
    })
})

app.post('/tododelete/:id/:i', (req, res) => {
    const qq = { _id: ObjectId(req.params.id) };
    const i = req.params.i;
    connection(async (db) => {
        const uus = await db.collection("user").updateOne(qq, { $unset: { ["todo." + i]: 1 } });
        const user = await db.collection("user").updateOne(qq, { $pull: { "todo": null } });
        res.send({ data: user });
    })
})

app.listen(3000, (err) => {
    if (err) throw err;
    console.log('server is running on port 3000');
});

